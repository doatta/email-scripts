#!/bin/sh
for email in "$@"; do
  if ! spamassassin -e < "$email" > /dev/null 2>&1; then
    mv "$email" /path/to/spam/folder/
  fi
done